post:
  tags:
    - Offmarket
  summary: Offmarket
  description: Posts offmarket listings to the database which runs the analysis algorithm 
  
  parameters:
    - name: access_token
      in: header
      description: JWT token generated after user login
      required: true
      schema:
        type: string

  requestBody:
    content:
      application/json:
        schema:
          type: object
          properties:
            location:
              type: object
              properties:
                address:
                  type: string
                  example: 14 River Run Circle, Sevierville, TN, USA
                  
                coords:
                  type: object
                  properties:
                    lat:
                      type: number
                      example: 29.8459072
                      
                    lng:
                      type: number
                      example: -99.8459072
           
            price:
              description: listing price of the property
              type: number
              examples:
                - 30000.0
            beds:
              description: number of bedrooms in the property
              type: number
              examples:
                - 3
            baths:
              description: number of bathrooms in the property
              type: number
              examples:
                - 2
                
            lot_sqft:
              description: total lot sqft of the property
              type: number
              examples:
                - 4160.0
                
            sqft:
              description: total sqft of the property
              type: number
              examples:
                - 2500.0
                
            arv:
              description: after repair value of the property - only available for the offmarket
              type: number
              examples:
                - 110000
                
            repairs:
              description: estimated repairs required for the property - only available for the offmarket
              type: number
              examples:
                - 12000
                
            type_tags:
              description: tags of the property - only available for the offmarket
              type: array
              examples:
                - [Wholesale Deal, Fix & Flip]
                
            repair_items:
              description: section of the property that requires repairs- only available for the offmarket
              type: array
              examples:
                - [Roofing]
        
  responses:
    '200':
      description: Success
      content:
        application/json:
          schema:
            type: object
            properties:
              zillow_url:
                description: zillow_url of the property posted
                type: string
                
              realtor_url:
                description: realtor_url of the property posted
                type: string
                
                
    '502':
      description: integerernal server error
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                description: message field that returns what is causing the integerernal server error
                type: string
                
get:
  tags:
    - Offmarket
  summary: Offmarket
  description: Gets all the offmarket listing posted by the user
  
  parameters:
    - name: access_token
      in: header
      description: JWT token generated after user login
      required: true
      schema:
        type: string
        
  responses:
    '200':
      description: Success
      content:
        application/json:
          schema:
            type: object
            properties:
              Data:
                description: listings returned
                type: array
                items:
                  $ref: ../components/schemas/Listing.yaml
                
                
    '502':
      description: integerernal server error
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                description: message field that returns what is causing the integerernal server error
                type: string
                
                
put:
  tags:
    - Offmarket
  summary: Offmarket
  description: update offmarket listing attributes
  
  parameters:
    - name: access_token
      in: header
      description: JWT token generated after user login
      required: true
      schema:
        type: string

  requestBody:
    content:
      application/json:
        schema:
          type: object
          properties:
            location:
              type: object
              properties:
                address:
                  type: string
                  example: 14 River Run Circle, Sevierville, TN, USA
                  
                coords:
                  type: object
                  properties:
                    lat:
                      type: number
                      example: 29.8459072
                      
                    lng:
                      type: number
                      example: -99.8459072
           
            price:
              description: listing price of the property
              type: number
              examples:
                - 30000.0
            beds:
              description: number of bedrooms in the property
              type: number
              examples:
                - 3
            baths:
              description: number of bathrooms in the property
              type: number
              examples:
                - 2
                
            lot_sqft:
              description: total lot sqft of the property
              type: number
              examples:
                - 4160.0
                
            sqft:
              description: total sqft of the property
              type: number
              examples:
                - 2500.0
                
            arv:
              description: after repair value of the property - only available for the offmarket
              type: number
              examples:
                - 110000
                
            repairs:
              description: estimated repairs required for the property - only available for the offmarket
              type: number
              examples:
                - 12000
                
            type_tags:
              description: tags of the property - only available for the offmarket
              type: array
              examples:
                - [Wholesale Deal, Fix & Flip]
                
            repair_items:
              description: section of the property that requires repairs- only available for the offmarket
              type: array
              examples:
                - [Roofing]
        
  responses:
    '200':
      description: Success
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                description: status message of API operation 
                type: string  
                
    '502':
      description: integerernal server error
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                description: message field that returns what is causing the integerernal server error
                type: string
                
                
delete:
  tags:
    - Offmarket
  summary: Offmarket
  description: deletes offmarket listing from the system
  
  parameters:
    - name: access_token
      in: header
      description: JWT token generated after user login
      required: true
      schema:
        type: string
        
    - name: listing_id
      in: query
      description: listing id of offmarket listing to be deleted
      required: true
      schema:
        type: string
        
  responses:
    '200':
      description: Success
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                description: status message of API operation 
                type: string
                enum:
                  - listing deleted
                  - listing not found
                
    '502':
      description: integerernal server error
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                description: message field that returns what is causing the integerernal server error
                type: string
                  